<?php
/**
 * Fetchr
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to integration@fetchr.us so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Fetchr Magento Extension to newer
 * versions in the future. If you wish to customize Fetchr Magento Extension (Fetchr Delivery) for your
 * needs please email to integration@fetchr.us.
 *
 * @author     Danish Kamal
 * @package    Fetchr Delivery
 * Used in creating options for Dropship/Fulfillment config value selection
 * @copyright  Copyright (c) 2018 Fetchr (https://fetchr.us)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
namespace Fetchr\Delivery\Cron;

class UpdateOrdersStatus
{
    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $_logger;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $_scopeConfig;

    protected $_orderFactory;

    protected $_orderRepository;

    /**
     * Constructor
     *
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Sales\Model\OrderFactory $orderFactory,
        \Magento\Sales\Model\OrderRepository $orderRepository
    ) {
        $this->_logger              = $logger;
        $this->_scopeConfig         = $scopeConfig;
        $this->_orderFactory        = $orderFactory;
        $this->_orderRepository     = $orderRepository;
    }

    public function execute()
    {
        //Check if Fetchr NOT enabled
        if(!$this->_scopeConfig->getValue('carriers/fetchr/active',\Magento\Store\Model\ScopeInterface::SCOPE_STORE)){
            return;
        }

        $accountType  = $this->_scopeConfig->getValue('carriers/fetchr/account_type',\Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        switch ($accountType) {
            case 'production':
                $baseurl = $this->_scopeConfig->getValue('fetchr_shipping/settings/productionurl',\Magento\Store\Model\ScopeInterface::SCOPE_STORE);
            break;
            case 'sandbox':
                $baseurl = $this->_scopeConfig->getValue('fetchr_shipping/settings/sandboxurl',\Magento\Store\Model\ScopeInterface::SCOPE_STORE);
            break;
        }

        $authorizationtoken  = $this->_scopeConfig->getValue('carriers/fetchr/authorizationtoken',\Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $addressid  = $this->_scopeConfig->getValue('carriers/fetchr/addressid',\Magento\Store\Model\ScopeInterface::SCOPE_STORE);

        $collection   =   $this->_orderFactory->create()->getCollection()
                                ->addFieldToFilter('main_table.status', array(
                                    array(
                                        'nin' => array(
                                            'complete',
                                            'closed',
                                            'canceled'
                                        ),
                                    ),
                                ));

        $result = $tracking_numbers = $order_tracking_numbers  = array();

        if ($collection->getData()) {
            foreach ($collection as $value) {
                $order = $this->_orderFactory->create()->load($value->getId());
                foreach($order->getShipmentsCollection() as $shipment) {
                    foreach($shipment->getAllTracks() as $tracknum) {
                        $order_tracking_numbers[$value->getId()][] = $tracknum->getNumber();
                    }
                }
            }
        }

        foreach ($order_tracking_numbers as $otn) {
            $tracking_numbers[] = end($otn);
        }

        $data   = array('authorizationtoken' => $authorizationtoken,
                        'addressid' => $addressid,
                        'data' =>  $tracking_numbers
                        );

        $data_string  = json_encode($data) ;
        $ch           = curl_init();
        $url          = $baseurl.'/api/get-status/';
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        $results = curl_exec($ch);
        $results = json_decode($results, true);

        foreach ($results['response'] as $result) {
            $xapiStatus  = $result['package_state'];
            $orderId    = $result['client_order_ref'];

            //Check if the order ID has client authorizationtoken as prefix
            if(strpos($orderId, '_') !== false){
              $oids     = explode('_', $orderId);
              $orderId  = end($oids);
            }

            $order      = $this->_orderFactory->create()->loadByIncrementId($orderId);
            $comments   = $order->getStatusHistoryCollection();

            //Get All The Comments
            foreach ($comments as $c) {
                $orderComments[$orderId][]    = $c->getData();
            }

            //Get Fetchr Comments Only
            foreach ($orderComments[$orderId] as $key => $comment) {
                $sw_fetchr    = strpos($comment['comment'], 'Fetchr');
                if($sw_fetchr != false){
                    $fetchrComments[$orderId][] = $comment;
                }
            }

            $lastFetchrComment  = $fetchrComments[$orderId][0]['comment'];
            $status_mapping = array(
                                'Scheduled for delivery',
                                'Order dispatched',
                                'Returned to Client',
                                'Customer care On hold',
                                );

            $statusdiff     = strpos($lastFetchrComment, $xapiStatus);
            $paymentType    = $order->getPayment()->getMethodInstance()->getCode();

            if(strstr($xapiStatus, 'Delivered') && $lastFetchrComment != null){
                $deliveryDate = explode(' ', $xapiStatus);

                $order->setState(\Magento\Sales\Model\Order::STATE_COMPLETE, true);
                $order->setStatus('complete');
                $order->addStatusHistoryComment('<strong>Delivered By Fetchr On: </strong>'.$deliveryDate[2], false);
                $this->_orderRepository->save($order);

                if($paymentType == 'cashondelivery' || $paymentType == 'phoenix_cashondelivery'){
                    $order->setBaseTotalInvoiced($order->getBaseGrandTotal());
                    $order->setBaseTotalPaid($order->getBaseGrandTotal());
                    $order->setTotalPaid($order->getBaseGrandTotal());
                }
                $order->save();

                foreach ($order->getInvoiceCollection() as $inv) {
                    $inv->setState(\Magento\Sales\Model\Order\Invoice::STATE_PAID)->save();
                }
            }elseif($xapiStatus != 'Order Created' && $statusdiff === false ){
                $order->setState(\Magento\Sales\Model\Order::STATE_PROCESSING, true);
                $order->setStatus('processing');
                $order->addStatusHistoryComment('<strong>Fetchr Status: </strong>'.$xapiStatus, false);
                $this->_orderRepository->save($order);
            }
        }
        $this->_logger->debug("orders statuses are updated now");
        return $results;

    }

}
