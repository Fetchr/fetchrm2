<?php
/**
 * Fetchr
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to integration@fetchr.us so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Fetchr Magento Extension to newer
 * versions in the future. If you wish to customize Fetchr Magento Extension (Fetchr Delivery) for your
 * needs please email to integration@fetchr.us.
 *
 * @author     Danish Kamal
 * @package    Fetchr Delivery
 * Used in creating options for Dropship/Fulfillment config value selection
 * @copyright  Copyright (c) 2018 Fetchr (https://fetchr.us)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Fetchr\Delivery\Helper;

use Magento\Shipping\Model\Config\Source\Allmethods;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    protected $_scopeConfig;
    protected $_allActiveMethods;

    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Shipping\Model\Config\Source\Allmethods $shippingMethods
    ) {
        parent::__construct($context);
        $this->_scopeConfig         = $scopeConfig;
        $this->_allActiveMethods    = $shippingMethods;
    }

    public function isEnabled()
    {
        return (bool) $this->_scopeConfig->getValue('fetchr/shipping/active', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function activShippingMethods()
    {
        //$methods = $this->_allActiveMethods->create();
        $activeMethods = $_allActiveMethods->toOptionArray(true);
        return $activeMethods;
    }
}
