<?php
/**
 * Fetchr
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to integration@fetchr.us so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Fetchr Magento Extension to newer
 * versions in the future. If you wish to customize Fetchr Magento Extension (Fetchr Delivery) for your
 * needs please email to integration@fetchr.us.
 *
 * @author     Danish Kamal
 * @package    Fetchr Delivery
 * Used in creating options for Dropship/Fulfillment config value selection
 * @copyright  Copyright (c) 2018 Fetchr (https://fetchr.us)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Fetchr\Delivery\Model\Carrier;

use Magento\Quote\Model\Quote\Address\RateRequest;
use Magento\Shipping\Model\Rate\Result;

class Fetchr extends \Magento\Shipping\Model\Carrier\AbstractCarrier implements
    \Magento\Shipping\Model\Carrier\CarrierInterface
{
    protected $_code = 'fetchr';
    protected $_trackFactory;
    protected $_trackErrorFactory;
    protected $_trackStatusFactory;

    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory $rateErrorFactory,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Shipping\Model\Rate\ResultFactory $rateResultFactory,
        \Magento\Quote\Model\Quote\Address\RateResult\MethodFactory $rateMethodFactory,
        \Magento\Shipping\Model\Tracking\ResultFactory $trackFactory,
        \Magento\Shipping\Model\Tracking\Result\ErrorFactory $trackErrorFactory,
        \Magento\Shipping\Model\Tracking\Result\StatusFactory $trackStatusFactory,
        array $data = []
    ) {
        $this->_rateResultFactory   = $rateResultFactory;
        $this->_rateMethodFactory   = $rateMethodFactory;
        $this->_trackFactory        = $trackFactory;
        $this->_trackStatusFactory  = $trackStatusFactory;
        $this->_trackErrorFactory   = $trackErrorFactory;
        parent::__construct($scopeConfig, $rateErrorFactory, $logger, $data);
    }

    /**
     * @return array
     */
    public function getAllowedMethods()
    {
        return ['fetchr' => $this->getConfigData('name')];
    }

    /**
     * @param RateRequest $request
     * @return bool|Result
     */
    public function collectRates(RateRequest $request)
    {
        $showInCheckout = $this->_scopeConfig->getValue('carriers/fetchr/show_in_checkout',\Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $fetchrshippingcharge    = $this->_scopeConfig->getValue('carriers/fetchr/fetchrshippingcharge',\Magento\Store\Model\ScopeInterface::SCOPE_STORE);

        if (!$showInCheckout) {
            return false;
        }

        /** @var \Magento\Shipping\Model\Rate\Result $result */
        $result = $this->_rateResultFactory->create();

        /** @var \Magento\Quote\Model\Quote\Address\RateResult\Method $method */
        $method = $this->_rateMethodFactory->create();

        $method->setCarrier('fetchr');
        $method->setCarrierTitle($this->getConfigData('title'));

        $method->setMethod('fetchr');
        $method->setMethodTitle($this->getConfigData('name'));

        /*you can fetch delivery price from different sources over some APIs, we used price from config.xml - xml node price*/
        $amount = $fetchrshippingcharge;
        //$amount = $this->getConfigData('price');
        $method->setPrice($amount);
        $method->setCost($amount);

        $result->append($method);

        return $result;
    }

    public function isTrackingAvailable()
    {
      return true;
    }

    public function getTrackingInfo($tracking)
    {
        try {
            $track = $this->_trackStatusFactory->create()
                    ->setUrl('https://track.fetchr.us/track/'.$tracking)
                    ->setCarrierCode($this->_code)
                    ->setCarrierTitle('Fetchr')
                    ->setTracking($tracking);
            return $track;
        } catch (Exception $e) {
            $this->_logger->debug('Tracking Popup Error: '.$e->getMessage());
        }
    }
}
