<?php
/**
 * Fetchr
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to integration@fetchr.us so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Fetchr Magento Extension to newer
 * versions in the future. If you wish to customize Fetchr Magento Extension (Fetchr Delivery) for your
 * needs please email to integration@fetchr.us.
 *
 * @author     Danish Kamal
 * @package    Fetchr Delivery
 * Used in creating options for Dropship/Fulfillment config value selection
 * @copyright  Copyright (c) 2018 Fetchr (https://fetchr.us)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Fetchr\Delivery\Model;

use Fetchr\Delivery\Model\XAPIResources;
use Magento\Sales\Model\Order;

class CODOrders {
	protected $_logger;
    protected $_scopeConfig;
    protected $_xapi;
    protected $_storeManager;
    protected $_invoiceManagement;
    protected $_transactionFactory;
    protected $_shipmentLoaderFactory;
    protected $_trackFactory;
    protected $_orderRepository;
    protected $_checkoutSession;
		protected $_countryFactory;

    public function __construct(
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Fetchr\Delivery\Model\XAPIResources $xapi,
        \Magento\Sales\Model\Service\InvoiceService $invoiceManagement,
        \Magento\Framework\DB\TransactionFactory $transactionFactory,
        \Magento\Shipping\Controller\Adminhtml\Order\ShipmentLoaderFactory $shipmentLoaderFactory,
        \Magento\Sales\Model\Order\Shipment\TrackFactory $trackFactory,
        \Magento\Sales\Model\OrderRepository $orderRepository,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Directory\Model\CountryFactory $countryFactory,
        array $data = [] )
    {
        $this->_logger = $logger;
        $this->_scopeConfig = $scopeConfig;
        $this->_storeManager = $storeManager;
        $this->_xapi = $xapi;
        $this->_invoiceManagement = $invoiceManagement;
        $this->_transactionFactory = $transactionFactory;
        $this->_shipmentLoaderFactory = $shipmentLoaderFactory;
        $this->_trackFactory = $trackFactory;
        $this->_orderRepository = $orderRepository;
        $this->_checkoutSession = $checkoutSession;
        $this->_countryFactory = $countryFactory;
    }

    public function pushCODOrder($order, $shipment='')
    {
        $paymentType        = 'COD';

        $storeTelephone     = $this->_scopeConfig->getValue('general/store_information/phone',\Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $storeAddress       = $this->_scopeConfig->getValue('general/store_information/address',\Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $shippingmethod     = explode('_', $order->getShippingMethod());
        $activeShippingMethods  = $this->_scopeConfig->getValue('carriers/fetchr/active_shipping_methods',\Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $activeShippingMethods  = explode(',', $activeShippingMethods);

        if(in_array($shippingmethod[0], $activeShippingMethods) || $shippingmethod[0] == 'fetchr')
        {
            try {
                    $items = $order->getAllVisibleItems();
                    foreach ($items as $item) {
                        //Replace Special characters in the items name
                        $item['name'] = strtr ($item['name'], array ('"' => ' Inch ', '&' => ' And '));
                        if($item['product_type'] == 'configurable'){
													$itemArray[] = array(
															'name' => $item['name'],
															'sku' => $item['sku'],
															'quantity' => intval($item['qty_ordered']),
															'price_per_unit' => $item->getPriceInclTax(),
													);

                        }elseif($item['product_type'] == "simple" && !$item->getParentItem()){

													$itemArray[] = array(
															'name' => $item['name'],
															'sku' => $item['sku'],
															'quantity' => intval($item['qty_ordered']),
															'price_per_unit' => $item->getPriceInclTax(),
													);
                        }

                    }

                    //get delivery address and delivery COD amount
                    $address        = $order->getShippingAddress()->getData();
										$grandTotal = $order->getGrandTotal();

                    //Handling Special chars in the address
                    foreach ($address as $key => $value) {
                        $address[$key] = strtr ($address[$key], array ('"' => ' ', '&' => ' And '));
                    }

                    $serviceType    = $this->_scopeConfig->getValue('carriers/fetchr/service_type',\Magento\Store\Model\ScopeInterface::SCOPE_STORE);
                    $authorizationtoken       = $this->_scopeConfig->getValue('carriers/fetchr/authorizationtoken',\Magento\Store\Model\ScopeInterface::SCOPE_STORE);
                    $addressid    = $this->_scopeConfig->getValue('carriers/fetchr/addressid',\Magento\Store\Model\ScopeInterface::SCOPE_STORE);
										$accountType    = $this->_scopeConfig->getValue('carriers/fetchr/account_type',\Magento\Store\Model\ScopeInterface::SCOPE_STORE);
										switch ($accountType) {
				                case 'production':
				                $trackingbaseurl = $this->_scopeConfig->getValue('fetchr_shipping/settings/productiontrackingurl',\Magento\Store\Model\ScopeInterface::SCOPE_STORE);
				                break;
				                case 'sandbox':
				                $trackingbaseurl = $this->_scopeConfig->getValue('fetchr_shipping/settings/sandboxtrackingurl',\Magento\Store\Model\ScopeInterface::SCOPE_STORE);
				            }
										$country_id = $address['country_id'];
        						$customercountry = $this->_countryFactory->create()->loadByCode($country_id);
        						$customer_country = $customercountry->getName();
                    $xapiOrderId     = $order->getIncrementId();
                    switch ($serviceType) {
                        case 'fulfillment':
												$dataXAPI = array(
														'data' => array(
																array(
																		'items' => $itemArray,
																		'warehouse_location' => array(
																				'id' => $addressid,
																		),
																		'details' => array(
																				'extra_fee' => $order->getShippingAmount(),
																				'discount' => '',
																				'customer_email' => $order['customer_email'],
																				'order_reference' => $xapiOrderId,
																				'customer_name' => $address['firstname'] . ' ' . $address['lastname'],
																				'payment_type' => $paymentType,
																				'customer_phone' => $address['telephone'],
																				'customer_city' => $address['city'],
																				'customer_country' => $customer_country,
																				'customer_address' => $address['street'],
																		),
																)
														),
												);
                        break;
                        case 'dropship':
												$dataXAPI = array(
														'client_address_id' => $addressid,
														'data' => array(
																array(
																		'order_reference' => $xapiOrderId,
																		'name' => $address['firstname'] . ' ' . $address['lastname'],
																		'email' => $order['customer_email'],
																		'phone_number' => $address['telephone'],
																		'address' => $address['street'],
																		'receiver_city' => $address['city'],
																		'receiver_country' => $customer_country,
																		'payment_type' => $paymentType,
																		'total_amount' => $grandTotal,
																		'description' => 'No',
																		'bag_count' => 1,
																		'comments' => '',
																),
														),
												);
                    }

                        $results  = $this->_xapi->_sendDataToXAPI($dataXAPI, $order->getIncrementId());
                        $comments = '';
												$response = json_decode($results,TRUE);

                        if($response['data']['0']['status'] == "error"){
                            $comments  .= '<strong>Fetchr Status: '.$response['data']['0']['status'].', </strong> Error Code: '.$response['data']['0']['error_code'].', Order was NOT pushed due to '.$response['data']['0']['message'].', trace_id: '.$response['data']['0']['trace_id'].', Please contact one of Fetchr\'s account managers and try again later';
                            $order->setStatus('pending');
                            $order->addStatusHistoryComment($comments, false);
                        }else{
                            // Setting The Comment in the Order view
                            if($serviceType == 'fulfillment' ){

                                $tracking_number    = $response['data']['0']['tracking_no'];

                                if($response['data']['0']['status'] == 'error'){
                                    $comments  .= '<strong>Fetchr Status: Failed <br>Order Not Pushed</strong></strong> in Fetchr system <br><strong>Error Code: '.$response['data']['0']['error_code'].'<br> Message: '.$response['data']['0']['message'].' <br>Please check with your Fetchr account manager</strong>';
                                    $order->setStatus('pending');
                                    $order->addStatusHistoryComment($comments, false);
                                }else{
                                    $comments  .= '<strong>Fetchr Status: Success <br>Order Pushed</strong> in <strong>Fetchr</strong> system <br><strong>Order Tracking No: </strong> '.$tracking_number.'<br><strong>Fetchr Tracking URL: </strong>'.$trackingbaseurl.$tracking_number;
                                    $order->setState(\Magento\Sales\Model\Order::STATE_PROCESSING, true);
                                    $order->setStatus('processing');
                                    $order->addStatusHistoryComment($comments, false);
                                    $this->_orderRepository->save($order);
                                }

                            }elseif ($serviceType == 'dropship') {
                                $tracking_number    = $response['data']['0']['tracking_no'];
                                $comments  .= '<strong>Fetchr Status: Success <br>Order Pushed</strong> in <strong>Fetchr</strong> system <br><strong>Order Tracking No: </strong> '.$tracking_number.'<br><strong> AWB Label:</strong> '.$response['data']['0']['awb_link'].' <br><strong>Fetchr Tracking URL: </strong>'.$trackingbaseurl.$tracking_number;
                                $order->setState(\Magento\Sales\Model\Order::STATE_PROCESSING, true);
                                $order->setStatus('processing');
                                $order->addStatusHistoryComment($comments, false);
                                $this->_orderRepository->save($order);
                            }
                        }

                        //COD Order Delivery And Invoicing
                        if($response['data']['0']['status'] == "success"){
                            $this->_checkoutSession->setOrderPushedXAPI(true);
                            try {
                                //Get Order Qty
                                $qty = array();
                                foreach ($order->getAllVisibleItems() as $item) {
                                    $product_id             = $item->getProductId();
                                    $Itemqty                = $item->getQtyOrdered() - $item->getQtyShipped() - $item->getQtyRefunded() - $item->getQtyCanceled();
                                    $qty[$item->getId()]    = $Itemqty;
                                }

                                //Invoicing
                                if($order->canInvoice()) {
                                    $invoice = $this->_invoiceManagement->prepareInvoice($order);
                                    $invoice->register();

                                    $invoice->setRequestedCaptureCase(\Magento\Sales\Model\Order\Invoice::CAPTURE_OFFLINE);
                                    $invoice->setState(1)
                                    ->save();

                                    $order->setBaseTotalInvoiced('0.0000');
                                    $order->setBaseTotalDue($order->getBaseGrandTotal());
                                    $order->setTotalPaid(0)
                                    ->setBaseTotalPaid(0)
                                    ->save();

                                    $transactionSave = $this->_transactionFactory->create()
                                    ->addObject($invoice)
                                    ->addObject($invoice->getOrder());

                                    $transactionSave->save();
                                }else{
                                    $this->_logger->debug('You can NOT create an invoice for this Order');
                                }

                                //Create Delivery When Auto Push Is OFF
                                if(!empty($shipment)){
                                    $trackdata = array();
                                    $trackdata['carrier_code']  = 'fetchr';
                                    $trackdata['title']         = 'Fetchr';
                                    $trackdata['url']           = $trackingbaseurl.$tracking_number;
                                    $trackdata['number']        = $tracking_number;

                                    $shipment->addTrack(
                                    $this->_trackFactory->create()
                                    ->setNumber($trackdata['number'])
                                    ->setUrl($trackdata['url'])
                                    ->setCarrierCode($trackdata['carrier_code'])
                                    ->setTitle($trackdata['title'])
                                    );
                                }else{
                                    //Create Delivery When Auto Push Is ON
                                    if ($order->canShip()) {
                                    	$shipmentLoader = $this->_shipmentLoaderFactory->create();
                                    	$shipmentData 	= $qty;
                                    	$shipmentLoader->setOrderId($order->getId());
                                    	$shipmentLoader->setShipment($shipmentData);
        															$shipment = $shipmentLoader->load();

        															$trackdata = array();
	                                    $trackdata['carrier_code']  = 'fetchr';
	                                    $trackdata['title']         = 'Fetchr';
	                                    $trackdata['url'] 			= $trackingbaseurl.$tracking_number;
	                                    $trackdata['number']        = $tracking_number;

                                      $shipment->addTrack(
						                    			$this->_trackFactory->create()
						                        	->setNumber($trackdata['number'])
						                        	->setUrl($trackdata['url'])
						                        	->setCarrierCode($trackdata['carrier_code'])
						                        	->setTitle($trackdata['title'])
						                					);
                                    } else {
                                        $this->_logger->debug('You can NOT create a shipment for this Order');
                                    }
                                }
                                if ($shipment) {
                                    $shipment->register();
                                    $shipment->getOrder()->setIsInProcess(true);
                                    $shipment->save();
                                    $shipmentTransaction = $this->_transactionFactory->create()
                                  	->addObject($shipment)
                                    ->addObject($shipment->getOrder());
                                    $shipmentTransaction->save();
                                }
                            }catch (Exception $e) {
                                $this->_logger->debug('Exception occurred during automatically Invoice and Ship this Order due to '.$e->getMessage());
                            }
                        }
                        // End COD Order Delivery And Invoicing
                        unset($dataXAPI, $itemArray);

            } catch(\Exception $e){
							$this->_logger->debug('Order is NOT Pushed due to '.$e->getMessage());
            }
        }
    }
}
