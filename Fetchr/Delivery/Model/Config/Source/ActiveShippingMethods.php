<?php
/**
 * Fetchr
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to integration@fetchr.us so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Fetchr Magento Extension to newer
 * versions in the future. If you wish to customize Fetchr Magento Extension (Fetchr Delivery) for your
 * needs please email to integration@fetchr.us.
 *
 * @author     Danish Kamal
 * @package    Fetchr Delivery
 * Used in creating options for Dropship/Fulfillment config value selection
 * @copyright  Copyright (c) 2018 Fetchr (https://fetchr.us)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Fetchr\Delivery\Model\Config\Source;

class ActiveShippingMethods implements \Magento\Framework\Option\ArrayInterface
{
    protected $_scopeConfig;
    protected $_allActiveMethods;
    protected $_logger;

    public function __construct(
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Shipping\Model\Config\Source\Allmethods $shippingMethods
    ) {
        $this->_scopeConfig         = $scopeConfig;
        $this->_allActiveMethods    = $shippingMethods;
        $this->_logger              = $logger;
    }

    public function toOptionArray()
    {
        $activeShippingMethods = $this->_allActiveMethods->toOptionArray(true);
        $activeMethods = array();
        foreach ($activeShippingMethods as $key => $shippingMethod) {
            if(isset($activeShippingMethods[$key]['label']) && !empty($activeShippingMethods[$key]['label'])){
                // echo "<pre>";print_r($shippingMethod['label']);die;
                $activeMethods[] = [
                    'value' =>$activeShippingMethods[$key]['label'],
                    'label' =>$activeShippingMethods[$key]['label'],
                ];
            }
        }
        return $activeMethods;
    }
}
