<?php
/**
 * Fetchr
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to integration@fetchr.us so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Fetchr Magento Extension to newer
 * versions in the future. If you wish to customize Fetchr Magento Extension (Fetchr Delivery) for your
 * needs please email to integration@fetchr.us.
 *
 * @author     Danish Kamal
 * @package    Fetchr Delivery
 * Used in creating options for Dropship/Fulfillment config value selection
 * @copyright  Copyright (c) 2018 Fetchr (https://fetchr.us)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Fetchr\Delivery\Model;

class XAPIResources {

    protected $_logger;
    protected $_scopeConfig;

    public function __construct(
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        array $data = [] )
    {
        $this->_logger          = $logger;
        $this->_scopeConfig     = $scopeConfig;
    }

    // public function _checkIfOrderIsPushed($orderId)
    // {
    //     $accountType  = $this->_scopeConfig->getValue('carriers/fetchr/account_type',\Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    //     $authorizationtoken  = $this->_scopeConfig->getValue('carriers/fetchr/authorizationtoken',\Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    //     $addressid  = $this->_scopeConfig->getValue('carriers/fetchr/addressid',\Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    //
    //     switch ($accountType) {
    //         case 'production':
    //             $baseurl = $this->_scopeConfig->getValue('fetchr_shipping/settings/productionurl',\Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    //         break;
    //         case 'Sandbox':
    //             $baseurl = $this->_scopeConfig->getValue('fetchr_shipping/settings/sandboxurl',\Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    //         break;
    //     }
    //
    //     $data   =   array(  'authorizationtoken' => $authorizationtoken,
    //                         'addressid' => $addressid,
    //                         'order_id' =>  $orderId
    //                 );
    //
    //     try{
    //         $data_string  = json_encode($data) ;
    //         $ch           = curl_init();
    //         $url          = $baseurl.'/api/getreport/';
    //
    //         curl_setopt($ch, CURLOPT_URL, $url);
    //         curl_setopt($ch, CURLOPT_POST, true);
    //         curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    //         curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
    //
    //         $results = curl_exec($ch);
    //         $results = json_decode($results, true);
    //
    //         return $results;
    //
    //     }catch (Exception $e) {
    //         echo (string) $e->getMessage();
    //     }
    // }

    public function _sendDataToXAPI($dataXAPI, $orderId)
    {
        $response = null;

        try {
            $accountType    = $this->_scopeConfig->getValue('carriers/fetchr/account_type',\Magento\Store\Model\ScopeInterface::SCOPE_STORE);
            $serviceType    = $this->_scopeConfig->getValue('carriers/fetchr/service_type',\Magento\Store\Model\ScopeInterface::SCOPE_STORE);
            $authorizationtoken       = $this->_scopeConfig->getValue('carriers/fetchr/authorizationtoken',\Magento\Store\Model\ScopeInterface::SCOPE_STORE);
            $addressid       = $this->_scopeConfig->getValue('carriers/fetchr/addressid',\Magento\Store\Model\ScopeInterface::SCOPE_STORE);
            switch ($accountType) {
                case 'production':
                $baseurl = $this->_scopeConfig->getValue('fetchr_shipping/settings/productionurl',\Magento\Store\Model\ScopeInterface::SCOPE_STORE);
                $trackingbaseurl = $this->_scopeConfig->getValue('fetchr_shipping/settings/productiontrackingurl',\Magento\Store\Model\ScopeInterface::SCOPE_STORE);
                break;
                case 'sandbox':
                $baseurl = $this->_scopeConfig->getValue('fetchr_shipping/settings/sandboxurl',\Magento\Store\Model\ScopeInterface::SCOPE_STORE);
                $trackingbaseurl = $this->_scopeConfig->getValue('fetchr_shipping/settings/sandboxtrackingurl',\Magento\Store\Model\ScopeInterface::SCOPE_STORE);
            }

            switch ($serviceType) {
                case 'fulfillment':
                $XAPIdata        = json_encode($dataXAPI, JSON_UNESCAPED_UNICODE);
                $url = $baseurl."fulfillment/";
                $ch = curl_init($url);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                $headers[] = 'Authorization: '.$authorizationtoken;
                $headers[] = 'Content-Type: application/json';
                $headers[] = 'xcaller: Magento 2 X1.1';
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $XAPIdata);
                $response = curl_exec ($ch);
                return $response;
                break;
                case 'dropship':
                $XAPIdata        = json_encode($dataXAPI, JSON_UNESCAPED_UNICODE);
                $url = $baseurl.'order/';
                $ch = curl_init($url);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                $headers[] = 'Authorization: '.$authorizationtoken;
                $headers[] = 'Content-Type: application/json';
                $headers[] = 'xcaller: Magento 2 X1.1';
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $XAPIdata);
                $response = curl_exec ($ch);
                return $response;
                break;
            }
        } catch (Exception $e) {
            echo (string) $e->getMessage();
        }
    }
}
