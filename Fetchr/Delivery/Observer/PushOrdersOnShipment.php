<?php
/**
 * Fetchr
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to integration@fetchr.us so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Fetchr Magento Extension to newer
 * versions in the future. If you wish to customize Fetchr Magento Extension (Fetchr Delivery) for your
 * needs please email to integration@fetchr.us.
 *
 * @author     Danish Kamal
 * @package    Fetchr Delivery
 * Used in creating options for Dropship/Fulfillment config value selection
 * @copyright  Copyright (c) 2018 Fetchr (https://fetchr.us)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Fetchr\Delivery\Observer;

use Magento\Framework\Event\ObserverInterface;

class PushOrdersOnShipment implements ObserverInterface{

    protected $_logger;
    protected $_scopeConfig;
    protected $_shipmentLoader;
    protected $_pushcod;
    protected $_pushcc;
    protected $_xapi;
    protected $_checkoutSession;

    public function __construct(
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Fetchr\Delivery\Model\CODOrders $pushCOD,
        \Fetchr\Delivery\Model\CCOrders $pushCC,
        \Fetchr\Delivery\Model\XAPIResources $xapi,
        \Magento\Checkout\Model\Session $checkoutSession,

        array $data = [] )
    {
        $this->_logger          = $logger;
        $this->_scopeConfig     = $scopeConfig;
        $this->_pushcod         = $pushCOD;
        $this->_pushcc          = $pushCC;
        $this->_xapi            = $xapi;
        $this->_checkoutSession = $checkoutSession;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $this->pushOrderAfterShipmentCreation($observer);
    }

    public function pushOrderAfterShipmentCreation($observer)
    {
        $shipment               = $observer->getEvent()->getShipment();
        $order                  = $shipment->getOrder();

        $shippingmethod         = $order->getShippingMethod();
        $paymentType            = $order->getPayment()->getMethodInstance()->getCode();

        // Get the selected delivery methods from the config of Fetchr Delivery
        // And Include them as they are fethcr.
        $activeShippingMethods  = $this->_scopeConfig->getValue('carriers/fetchr/active_shipping_methods',\Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $activeShippingMethods  = explode(',', $activeShippingMethods);

        if(strstr($paymentType, 'paypal')){
            $paymentType = 'paypal';
        }
        switch ($paymentType) {
            case 'cashondelivery':
                $paymentType    = 'COD';
            break;
            case 'ccsave':
                $paymentType    = 'CCOD';
            break;
            case 'paypal':
            default:
                $paymentType    = 'CC';
            break;
        }

        $shippingmethod     = explode('_', $shippingmethod);

        $authorizationtoken       = $this->_scopeConfig->getValue('carriers/fetchr/authorizationtoken',\Magento\Store\Model\ScopeInterface::SCOPE_STORE);

        $OrderPushedToXAPI = $this->_checkoutSession->getOrderPushedXAPI();
        $xapiOrderStatus['order_status'] ='';
        if($OrderPushedToXAPI == false){
          $xapiOrderStatus['order_status'] == 'Order Not Found';
              if( (in_array($shippingmethod[0], $activeShippingMethods) || $shippingmethod[0] == 'fetchr') && $paymentType == 'COD' ){
                  return $this->_pushcod->pushCODOrder($order, $shipment);
              }elseif( (in_array($shippingmethod[0], $activeShippingMethods) || $shippingmethod[0] == 'fetchr') && ($paymentType == 'CCOD' || $paymentType == 'CC') ){
                  return $this->_pushcc->pushCCOrder($order, $shipment, $paymentType);
              }

        }else{
            $xapiOrderStatus['order_status']  = 'Order Created';
            $this->_checkoutSession->unsOrderPushedXAPI();
        }

    }
}
