<?php
/**
 * Fetchr
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to integration@fetchr.us so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Fetchr Magento Extension to newer
 * versions in the future. If you wish to customize Fetchr Magento Extension (Fetchr Delivery) for your
 * needs please email to integration@fetchr.us.
 *
 * @author     Danish Kamal
 * @package    Fetchr Delivery
 * Used in creating options for Dropship/Fulfillment config value selection
 * @copyright  Copyright (c) 2018 Fetchr (https://fetchr.us)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Fetchr\Delivery\Observer;

use Magento\Framework\Event\ObserverInterface;
use Fetchr\Delivery\Model\CCOrders;

class PushOrdersOnInvoicing implements ObserverInterface{

    protected $_logger;
    protected $_scopeConfig;
    protected $_pushcc;

    public function __construct(
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Fetchr\Delivery\Model\CCOrders $pushCC,
        array $data = [] )
    {
        $this->_logger          = $logger;
        $this->_scopeConfig     = $scopeConfig;
        $this->_pushcc          = $pushCC;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        /** @var Customer $customer */

        $this->pushOrderAfterInvoiceCreation($observer);
    }

    public function pushOrderAfterInvoiceCreation($observer)
    {
    	//Check IF the Auto Push Is Enabled
	    $autoCCPush     = $this->_scopeConfig->getValue('carriers/fetchr/autoccpush',\Magento\Store\Model\ScopeInterface::SCOPE_STORE);
	    $invoice        = $observer->getEvent()->getInvoice();
	    $order          = $invoice->getOrder();
	    $paymentType    = $order->getPayment()->getMethodInstance()->getCode();

	    if(strstr($paymentType, 'paypal')){
	        $paymentType = 'paypal';
	    }
	    switch ($paymentType) {
	        case 'cashondelivery':
	        case 'phoenix_cashondelivery':
	            $paymentType    = 'COD';
	        break;
	        case 'ccsave':
	            $paymentType    = 'CCOD';
	        break;
	        case 'paypal':
	        default:
	            $paymentType    = 'CC';
	        break;
	    }

	    if($autoCCPush == true && ($paymentType == 'CCOD' || $paymentType == 'CC') ){
	        return $this->_pushcc->pushCCOrder($order, '', $paymentType);
	    }
    }
}
